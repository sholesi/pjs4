let express = require('express');
let app = express();
let bodyParser = require('body-parser');
var ctrl_Mission1 = require('./controllers/ctrl_Mission1');
var connect_BD = require('./models/connect_BD');


// Moteur de template
app.set('view engine', 'ejs');

// Controllers
ctrl_Mission1(app);

// Models
connect_BD(app);

// Middleware --> CSS, JS, Image...  ==> ces fichiers doivent être stockés dans le dossier public
app.use('/assets', express.static('public'));
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

// Routes
app.get('/', (request, response) => {
    response.render('index', { test: 'salut' });
});

app.post('/', (request, response) => {
    console.log(request.body);
})

// Server créer sur le port 8080 --> http://localhost:8080/
app.listen(8080);
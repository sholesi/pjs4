********************************************

Pour executer :

1. Dans l'invite de commande, aller dans la racine du projet (au niveau de server.js)
2. Taper npm run start
3. Dans le navigateur --> http://localhost:8080/

********************************************


********************************************

Liste commande utile avec "npm" : 

npm run start --> lancer le serveur

npm init --> Pour initialiser notre projet et avoir le fichier package.json
npm i --save express --> Pour installer "express.js" 
npm i --save nodemon --> Pour installer "nodemon" => utile pour le serveur
npm i --save ejs --> Pour installer ejs => extension template

********************************************


********************************************

redirection :

response.render('pages/index', {test: 'salut'}); --> redirection vers la page 'index.ejs' + envoie de la paramètre 'test'

********************************************
ejs :
<%= variable %> --> pour lire une variable

********************************************


********************************************

Fichier et dossier utile pour l'instant :

server.js
footer.ejs
header.ejs
index.ejs
public/Image
public/Js

********************************************

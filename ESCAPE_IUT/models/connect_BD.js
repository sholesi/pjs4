module.exports = function (app) {
    var mysql = require('mysql');

    var bd = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "pweb19_rakotonirina"
    });

    bd.connect((err) => {
        if (err) {
            throw err;
        }
        console.log('Connected');
    })

    app.all('/getEtudiant', (req, res) => {
        let sql = 'SELECT prenom FROM etudiant WHERE id_etu = 1';
        let query = bd.query(sql, (err, results) => {
            if (err) {
                throw err;
            }
            console.log(results);
            res.render('mission/testBD', { test: results[0] });
        });
    });

};
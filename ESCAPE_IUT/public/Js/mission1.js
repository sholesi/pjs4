const config = {
    width: 1440,
    height: 800,
    type: Phaser.AUTO,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false,
            gravity: {
                y: 0
            }
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
}

var game = new Phaser.Game(config);
var dude;
var clavier;
var mur;
var tableCoté1;
var tableCoté2;
var tableProf;
var tableProf2;
var tableOrdi;
var table1;
var table2;
var table3;
var salleVide;
var poubelle1;
var poubelle2;



/**
 * Preload sert à generer les éléments externe (Image, musique, vidéo...)
 */
function preload() {

    this.load.image('mur', '/assets/Images/Décors/mur.jpg');
    this.load.image('tableCoté', '/assets/Images/Décors/tablecoté.png');
    this.load.image('tableProf', '/assets/Images/Décors/tableProf.png');
    this.load.image('tableOrdi', '/assets/Images/Décors/tableOrdi.png');
    this.load.image('table', '/assets/Images/Décors/tables.png');
    this.load.image('salleVide', '/assets/Images/Décors/salleVide.jpg');
    this.load.image('poubelle', '/assets/Images/Décors/poubelle.png');

    this.load.spritesheet('dude', '/assets/Images/Personnages/dude.png', { frameWidth: 32, frameHeight: 48 });

}

/**
 * Création des éléments générer
 */
function create() {

    salleVide = this.physics.add.image(720, 400, 'salleVide').setImmovable(true);

    mur = this.physics.add.image(720, 84, 'mur').setImmovable(true);

    tableCoté1 = this.physics.add.image(58, 524, 'tableCoté').setImmovable(true);
    tableCoté2 = this.physics.add.image(1380, 520, 'tableCoté').setImmovable(true);

    tableProf = this.physics.add.image(702, 250, 'tableProf').setImmovable(true);
    tableProf2 = this.physics.add.image(702, 250, 'tableProf').setImmovable(true);

    table1 = this.physics.add.image(719, 408, 'table').setImmovable(true);
    table2 = this.physics.add.image(719, 570, 'table').setImmovable(true);
    table3 = this.physics.add.image(717, 715, 'table').setImmovable(true);

    tableOrdi = this.physics.add.image(1063, 187, 'tableOrdi').setImmovable(true);

    poubelle1 = this.physics.add.image(212, 170, 'poubelle').setImmovable(true);
    poubelle2 = this.physics.add.image(1225, 173, 'poubelle').setImmovable(true);

    dude = this.physics.add.sprite(400, 250, 'dude');
    dude.setScale(1.5);
    dude.setBounce(0.2);
    dude.setCollideWorldBounds(true);

    //Gestion des frames du personnage
    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'turn',
        frames: [{ key: 'dude', frame: 4 }],
        frameRate: 20
    });

    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
        frameRate: 10,
        repeat: -1
    });

    //Gestion avec les touches du claviers 
    clavier = this.input.keyboard.createCursorKeys(); //Haut, Bas, Shift, Espace, Gauche, Droite
    toucheA = this.input.keyboard.addKey('A');
}

/**
 * Modification des éléments générer
 */
function update() {

    // Gestion des collisions 
    this.physics.add.collider(dude, table1);
    this.physics.add.collider(dude, table2);
    this.physics.add.collider(dude, table3);
    this.physics.add.collider(dude, tableCoté1);
    this.physics.add.collider(dude, tableCoté2);
    this.physics.add.collider(dude, tableOrdi);
    this.physics.add.collider(dude, tableProf);
    this.physics.add.collider(dude, tableProf2);
    this.physics.add.collider(dude, poubelle1);
    this.physics.add.collider(dude, poubelle2);
    this.physics.add.collider(dude, mur);

    dude.body.setSize(28, 40, 0, 0);
    dude.body.setOffset(0, 10);
    tableOrdi.body.setSize(245, 38);
    tableOrdi.body.setOffset(0, 50);
    poubelle1.body.setSize(52, 20);
    poubelle1.body.setOffset(0, 5);
    poubelle2.body.setSize(52, 20);
    poubelle2.body.setOffset(0, 5);
    tableProf.body.setSize(223, 40);
    tableProf.body.setOffset(0, 40);
    tableProf2.body.setSize(65, 40);
    tableProf2.body.setOffset(80, 0);
    mur.body.setSize(1600, 115);
    mur.body.setOffset(0, 0);
    table1.body.setSize(1010, 30);
    table1.body.setOffset(0, 0);
    table2.body.setSize(1010, 30);
    table2.body.setOffset(0, 0);
    table3.body.setSize(1010, 30);
    table3.body.setOffset(0, 0);

    seDeplacer();

}

/**
 * Gestion deplacement du perso
 */
function seDeplacer() {
    dude.body.velocity.x = 0;
    dude.body.velocity.y = 0;

    if (clavier.left.isDown) {
        dude.setVelocityX(-250);
        dude.anims.play('left', true);
    }
    else if (clavier.right.isDown) {
        dude.setVelocityX(250);
        dude.anims.play('right', true);
    } else if (clavier.up.isDown) {
        dude.setVelocityY(-250);
    } else if (clavier.down.isDown) {
        dude.setVelocityY(250);
    }
    else {
        dude.setVelocityX(0);
        dude.anims.play('turn');
    }

}



